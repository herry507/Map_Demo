package com.spock.mapdemo.utils;


/**
 * Created by spock on 16-3-2.
 */
public class Log {
    private static final String TAG = "MAP_DEMO";
    private static boolean DEBUG = true;

    public static void d(String className, String content) {
        if (DEBUG) {
            android.util.Log.d(TAG, "[" + className + "]---------" + content);
        }
    }

    public static void e(String className, String content) {
        if (DEBUG) {
            android.util.Log.e(TAG, "[" + className + "]######" + content + "######");
        }
    }

    public static void e(String className, String content, Throwable exception) {
        if (DEBUG) {
            android.util.Log.e(TAG, "[" + className + "]---------" + content, exception);
        }
    }

    public static void log(String message, Object... args) {
        if (DEBUG) {
            android.util.Log.d(TAG, args.length == 0 ? message : String.format(message, args));
        }
    }

    public static void log(Throwable error, String message, Object... args) {
        if (DEBUG) {
            android.util.Log.d(TAG, args.length == 0 ? message : String.format(message, args), error);
        }
    }
}


